package currencyConverter;

/**
 * A currency converter model which do calculation of the converter.
 * @author Chinthiti Wisetsombat
 *
 */
public class ConverterModel {

	/**
	 * Convert value of one currency to another.
	 * @param value is the amount of money in original currency.
	 * @param from is the original currency.
	 * @param to is the currency that value will be converted to.
	 * @return return amount of money in currency that value is converted to.
	 */
	public double convertCurrency(double value, Currency from, Currency to){
		return value * from.getValue() / to.getValue();
	}
}
