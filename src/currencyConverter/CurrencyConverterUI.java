package currencyConverter;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * A Graphical user interface of the currency converter.
 * @author Chinthiti Wisetsombat
 *
 */
public class CurrencyConverterUI implements Runnable{

	private JFrame frame;
	private JTextField txt_leftValue;
	private JComboBox combo_leftCurrency;
	private JLabel label;
	private JTextField txt_rightValue;
	private JComboBox combo_rightCurrency;
	private JButton btn_convert;


	/**
	 * Create the application.
	 */
	public CurrencyConverterUI() {
		initialize();
		frame.pack();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Simple Currency Converter");

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		txt_leftValue = new JTextField();
		panel.add(txt_leftValue);
		txt_leftValue.setColumns(10);
		
		
		Currency[] currencies1 = Currency.values();
		combo_leftCurrency = new JComboBox(currencies1);
		panel.add(combo_leftCurrency);
		
		label = new JLabel(" <---->");
		panel.add(label);
		
		txt_rightValue = new JTextField();
		panel.add(txt_rightValue);
		txt_rightValue.setColumns(10);
		
		Currency[] currencies2 = Currency.values();
		combo_rightCurrency = new JComboBox(currencies2);
		panel.add(combo_rightCurrency);
		
		btn_convert = new JButton("Convert");
		panel.add(btn_convert);
		
	}
	
	/**
	 * add an ActionListener to convert button.
	 * @param buttonListener is an ActionListener that will be added to convert button.
	 */
	public void addConvertButtonListener(ActionListener buttonListener){
		btn_convert.addActionListener(buttonListener);
	}
	
	/**
	 * Return the text in the left textfield.
	 * @return text in the left textfield.
	 */
	public String getLeftValue(){
		return txt_leftValue.getText();
	}
	
	/**
	 * Return the text in the right textfield.
	 * @return text in the right textfield.
	 */
	public String getRightValue(){
		return txt_rightValue.getText();
	}
	
	/** 
	 * Return Currency that is selected in the left combobox.
	 * @return Currency that is selected in the left combobox.
	 */
	public Currency getLeftCurrency(){
		return (Currency) combo_leftCurrency.getSelectedItem();
	}
	
	/** 
	 * Return Currency that is selected in the right combobox.
	 * @return Currency that is selected in the right combobox.
	 */
	public Currency getRightCurrency(){
		return (Currency) combo_rightCurrency.getSelectedItem();
	}
	
	
	/**
	 * Set a String to be display on left textfield.
	 * @param value is a String that will be displayed.
	 */
	public void setLeftValue(String value){
		txt_leftValue.setText(value);
	}
	
	/**
	 * Set a String to be display on right textfield.
	 * @param value is a String that will be displayed.
	 */
	public void setRightValue(String value){
		txt_rightValue.setText(value);
	}

	@Override
	public void run() {
		frame.setVisible(true);
	}
}
