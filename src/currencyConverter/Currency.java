package currencyConverter;

/**
 * An enum of currency to be used with CurrencyConverter.
 * @author Chinthiti Wisetsombat
 *
 */
public enum Currency {
	
	US_DOLLAR("US dollar", 32.70),
	EURO("Euro", 35.45),
	SINGAPORE_DOLLAR("Singapore dollar", 24.53),
	THAI_BAHT("Thai baht", 1);
	
	/** Currency name. */
	private String name;
	/** Value of the currency in Thai baht. */
	private double value;
	
	
	Currency(String name, double value){
		this.name = name;
		this.value = value;
	}
	
	/**
	 * Return value of the currency in Thai baht.
	 * @return value of the currency in Thai baht.
	 */
	public double getValue() {
		return value;
	}
	
	@Override
	public String toString(){
		return name;
	}
	
}
