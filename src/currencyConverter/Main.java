package currencyConverter;

/**
 * An application class that run the whole program.
 * @author Chinthiti Wisetsombat
 *
 */
public class Main {

	public static void main(String[]args){
		ConverterController controller = new ConverterController();
		controller.run();
	}
}
