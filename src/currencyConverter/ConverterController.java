package currencyConverter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingUtilities;

/**
 * A Controller of CurrencyConverter which is the bridge that connect Model and UI together.
 * @author Chinthiti Wisetsombat
 *
 */
public class ConverterController implements Runnable{

	/** The user interface of the converter. */
	private CurrencyConverterUI ui;
	/** The model that do calculation in the converter. */
	private ConverterModel model;
	
	/**
	 * A constructor for the ConverterController.
	 */
	public ConverterController(){
		ui = new CurrencyConverterUI();
		model = new ConverterModel();
		ConvertHandler convertHandler = new ConvertHandler();
		ui.addConvertButtonListener(convertHandler);
	}
	
	/** inner class for handling convert button action */
	private class ConvertHandler implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			String leftValue = ui.getLeftValue();
			String rightValue = ui.getRightValue();
			Currency leftCurrency = ui.getLeftCurrency();
			Currency rightCurrency = ui.getRightCurrency();
			
			double result;
			if(!leftValue.trim().equals("")){
				double value = Double.parseDouble(leftValue);
				result = model.convertCurrency(value, leftCurrency, rightCurrency);
				ui.setRightValue(String.format("%.2f", result));
				
			} else {
				double value = Double.parseDouble(rightValue);
				result = model.convertCurrency(value, rightCurrency, leftCurrency);
				ui.setLeftValue(String.format("%.2f", result));
			}
		}
		
	}


	@Override
	public void run() {
		SwingUtilities.invokeLater(ui);
	};
}
