package mvcPattern;

/**
 * Runs the BMI calculator application.
 * @author Thongrapee Panyapatiphan
 *
 */
public class BMICalculator {
	
	public static void main(String[] args){
		
		// Create model and view of the application
		CalculatorModel model = new CalculatorModel();
		CalculatorView view = new CalculatorView();
		
		// Give model and view to the controller
		CalculatorController controller = new CalculatorController( model, view );
		
		// Show the grahpical user interface
		view.setVisible(true);
		
	}

}
