package mvcPattern;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The controller of BMI calculator application, have access of both model and view of the application.
 * Collects data from the view, and give data to model for calculation.
 * @author Thongrapee Panyapatiphan
 *
 */
public class CalculatorController {
	
	private CalculatorModel model;
	private CalculatorView view;
	
	/**
	 * Create instance of controller containing model, and view of the application.
	 * @param model is the model of the application
	 * @param view is the graphical user interface
	 */
	public CalculatorController( CalculatorModel model, CalculatorView view ) {
		this.model = model;
		this.view = view;
		
		this.view.addCalculateBtnListener(new CalculateBtnListener());
	}
	
	private class CalculateBtnListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			double weight, height;
			
			try {
				weight = view.getWeightInput();
				height = view.getHeightInput();
				
				model.calculateBMI(weight, height);
				
				view.displayResult(String.format("%.2f", model.getBodyMassIndex()));
			} catch (NumberFormatException ex){ }
			
		}
		
	}

}
