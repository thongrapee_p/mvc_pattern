package mvcPattern;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * The view of BMI calculator application, 
 * responsible for collecting neccessary information and display computed results
 * @author Thongrapee Panyapatiphan
 *
 */
public class CalculatorView extends JFrame {
	
	JLabel heightLbl = new JLabel("Enter your height");
	JLabel weightLbl = new JLabel("Enter your weight");
	JTextField heightField = new JTextField(5);
	JTextField weightField = new JTextField(5);
	JButton calculateBtn = new JButton("Calculate");
	JTextField resultField = new JTextField(5);
	
	public CalculatorView(){
		super("BMI Calculator");
		this.setSize( 600, 60 );
		this.setLayout(new FlowLayout());
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		resultField.setEditable(false);
		add(heightLbl);
		add(heightField);
		add(weightLbl);
		add(weightField);
		add(calculateBtn);
		add(resultField);
	}
	
	/**
	 * Get weight from the user input field.
	 * @return weight input by the user
	 */
	public double getWeightInput(){
		return Double.parseDouble(weightField.getText());
	}
	
	/**
	 * Get height from the user input field.
	 * @return height input by the user
	 */
	public double getHeightInput(){
		return Double.parseDouble(heightField.getText());
	}
	
	/**
	 * Display result in the result display field
	 * @param result is a string of result that will be displayed
	 */
	public void displayResult(String result){
		resultField.setText(result);
	}
	
	/**
	 * Add actionListener to the calculate button.
	 * @param actionListener is an actionListener
	 */
	public void addCalculateBtnListener(ActionListener actionListener){
		calculateBtn.addActionListener(actionListener);
	}
	
	

}
