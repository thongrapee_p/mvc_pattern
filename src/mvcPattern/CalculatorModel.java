package mvcPattern;

/**
 * The model of BMI calculator application responsible for calculating value of BMI.
 * @author Thongrapee Panyapatiphan
 *
 */
public class CalculatorModel {
	
	private double bodyMassIndex;
	
	/**
	 * Calculate the value of BMI.
	 * @param weight is weight of a person in kilograms
	 * @param height is height of a person in meters
	 */
	public void calculateBMI( double weight, double height ) {
		bodyMassIndex = weight / ( height*height );
	}
	
	/**
	 * Get computed result.
	 * @return Body Mass Index(BMI)
	 */
	public double getBodyMassIndex(){
		return bodyMassIndex;
	}
	

}
